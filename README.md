Guru PMEGuru
O Fantásticol Gerador de Política de Privacidade
Nome da empresa, email e endereço = Política de Privacidade Pronta!

SEÇÃO 1 - O QUE FAREMOS COM ESTA INFORMAÇÃO?
Quando você realiza alguma transação com nossa loja, como parte do processo de compra e venda, coletamos as informações pessoais que você nos dá tais como: nome, e-mail e endereço.

Quando você acessa nosso site, também recebemos automaticamente o protocolo de internet do seu computador, endereço de IP, a fim de obter informações que nos ajudam a aprender sobre seu navegador e sistema operacional.

Email Marketing será realizado apenas caso você permita. Nestes emails você poderá receber notícia sobre nossa loja, novos produtos e outras atualizações.

SEÇÃO 2 - CONSENTIMENTO
Como vocês obtêm meu consentimento?

Quando você fornece informações pessoais como nome, telefone e endereço, para completar: uma transação, verificar seu cartão de crédito, fazer um pedido, providenciar uma entrega ou retornar uma compra. Após a realização de ações entendemos que você está de acordo com a coleta de dados para serem utilizados pela nossa empresa.

Se pedimos por suas informações pessoais por uma razão secundária, como marketing, vamos lhe pedir diretamente por seu consentimento, ou lhe fornecer a oportunidade de dizer não.

E caso você queira retirar seu consentimento, como proceder?

Se após você nos fornecer seus dados, você mudar de ideia, você pode retirar o seu consentimento para que possamos entrar em contato, para a coleção de dados contínua, uso ou divulgação de suas informações, a qualquer momento, entrando em contato conosco em adegasp.rafael@gmail.com ou nos enviando uma correspondência em: Nosso App Adail Alves Silva, 15

SEÇÃO 3 - DIVULGAÇÃO
Podemos divulgar suas informações pessoais caso sejamos obrigados pela lei para fazê-lo ou se você violar nossos Termos de Serviço.

SEÇÃO 4 - SERVIÇOS DE TERCEIROS
No geral, os fornecedores terceirizados usados por nós irão apenas coletar, usar e divulgar suas informações na medida do necessário para permitir que eles realizem os serviços que eles nos fornecem.

Entretanto, certos fornecedores de serviços terceirizados, tais como gateways de pagamento e outros processadores de transação de pagamento, têm suas próprias políticas de privacidade com respeito à informação que somos obrigados a fornecer para eles de suas transações relacionadas com compras.

Para esses fornecedores, recomendamos que você leia suas políticas de privacidade para que você possa entender a maneira na qual suas informações pessoais serão usadas por esses fornecedores.

Em particular, lembre-se que certos fornecedores podem ser localizados em ou possuir instalações que são localizadas em jurisdições diferentes que você ou nós. Assim, se você quer continuar com uma transação que envolve os serviços de um fornecedor de serviço terceirizado, então suas informações podem tornar-se sujeitas às leis da(s) jurisdição(ões) nas quais o fornecedor de serviço ou suas instalações estão localizados.

Como um exemplo, se você está localizado no Canadá e sua transação é processada por um gateway de pagamento localizado nos Estados Unidos, então suas informações pessoais usadas para completar aquela transação podem estar sujeitas a divulgação sob a legislação dos Estados Unidos, incluindo o Ato Patriota.

Uma vez que você deixe o site da nossa loja ou seja redirecionado para um aplicativo ou site de terceiros, você não será mais regido por essa Política de Privacidade ou pelos Termos de Serviço do nosso site.

Links

Quando você clica em links na nossa loja, eles podem lhe direcionar para fora do nosso site. Não somos responsáveis pelas práticas de privacidade de outros sites e lhe incentivamos a ler as declarações de privacidade deles.

SEÇÃO 5 - SEGURANÇA
Para proteger suas informações pessoais, tomamos precauções razoáveis e seguimos as melhores práticas da indústria para nos certificar que elas não serão perdidas inadequadamente, usurpadas, acessadas, divulgadas, alteradas ou destruídas.

Se você nos fornecer as suas informações de cartão de crédito, essa informação é criptografada usando tecnologia "secure socket layer" (SSL) e armazenada com uma criptografia AES-256. Embora nenhum método de transmissão pela Internet ou armazenamento eletrônico é 100% seguro, nós seguimos todos os requisitos da PCI-DSS e implementamos padrões adicionais geralmente aceitos pela indústria.

SEÇÃO 6 - ALTERAÇÕES PARA ESSA POLÍTICA DE PRIVACIDADE
Reservamos o direito de modificar essa política de privacidade a qualquer momento, então por favor, revise-a com frequência. Alterações e esclarecimentos vão surtir efeito imediatamente após sua publicação no site. Se fizermos alterações de materiais para essa política, iremos notificá-lo aqui que eles foram atualizados, para que você tenha ciência sobre quais informações coletamos, como as usamos, e sob que circunstâncias, se alguma, usamos e/ou divulgamos elas.

Se nossa loja for adquirida ou fundida com outra empresa, suas informações podem ser transferidas para os novos proprietários para que possamos continuar a vender produtos para você.

Baixar Política de Privacidade (PDF) Copiar o texto gerado
Isenção de responsabilidade legal
Estes modelos de exemplo não são aconselhamento jurídico e, ao utilizá-los, você concorda com essa isenção de responsabilidade. Os materiais a seguir são apenas para fins informativos e não constituem publicidade, uma solicitação ou aconselhamento jurídico. Você deve consultar aconselho jurídico independente antes da publicação desses acordos. Você deve ler as informações geradas com cuidado e modificar, apagar ou adicionar todas e quaisquer áreas, conforme necessário. Utilização, acesso ou transmissão de tais materiais e informações ou qualquer um dos links contidos neste documento não se destina a criar, e receber do mesmo não constitui formação de, uma relação advogado-cliente entre o GURU PME e o usuário ou navegador. Você não deve contar com esta informação para qualquer finalidade, sem procurar aconselhamento jurídico de um advogado licenciado em seu estado ou província. As informações contidas são fornecidas apenas como informação geral e podem ou não podem refletir os desenvolvimentos jurídicos mais atuais; consequentemente, não é prometido ou garantido que a informação seja correta ou completa. O GURU PME se isenta expressamente a qualquer responsabilidade em relação a quaisquer ações tomadas ou não tomadas, com base em qualquer um ou todos os conteúdos deste site. Além disso, o GURU PME não necessariamente endossa e não é responsável por qualquer conteúdo de terceiros que podem ser acessados através desta informação.

Vale ressaltar que o usuário fica ciente que se por ventura houver qualquer acionamento judicial ou extrajudicial que envolva qualquer assunto ou relação que esteja relacionado aos modelos ou interligados a ele, o GURU PME se isenta de qualquer responsabilidade ficando o usuário totalmente responsável, por qualquer responsabilidade ou reflexo que o acesso a nossa "plataforma" possa acarretar.

Como o Guru PME pode ajudar a sua empresa?
Tenha acesso a ferramentas gratuitas que vão facilitar a vida da sua empresa

Gerador de Política de troca para e-commerce
Evite problemas futuros para o seu e-commerce criando a sua política de troca de mercadorias

GERAR POLÍTICA DE TROCA
Gerador de Slogan para Empresas
Ter um slogan da sua empresa é tão importante quanto ter um nome! Clique aqui e gere logo o seu

GERAR SLOGAN
Gerador de Modelo de Timbrado para Empresa
O papel timbrado é uma peça fundamental na adequação da identidade visual do seu negócio.

GERAR MODELO DE TIMBRADO
Gerador de Termos para Compras em Site
Garanta que o seu e-commerce esteja adequado às leis de compra na internet usando essa ferramenta

GERAR TERMOS DE COMPRAS
Gerador de Política de Reembolso
Esteja preparado para reembolsar seus clientes quando for necessário! Crie Agora

CRIAR POLÍTICA DE REEMBOLSO
Gerador de Contrato de Confidencialidade
Prepare um documento de confidencialidade robusto para se prevenir de quaisquer dores de cabeça

GERAR CONFIDENCIALIDADE
Gerador de Política de Privacidade
O documento de política de privacidade é essencial para qualquer negócio. Você ainda não possui um?

CRIAR POLÍTICA DE PRIVACIDADE
Gerador de Nome de Empresa
Ainda não definiu o nome da sua empresa? Nesta ferramenta você finalmente resolverá este problema

GERAR NOME DE EMPRESA
Gerador de Carta de Demissão
Precisa demitir alguém e não preparou o modelo da carta de demissão? Preencha os campos da ferramenta e está tudo resolvido

GERAR CARTA DE DEMISSÃO
Relatório de ROI de campanha digital
Tenha uma previsão de qual será o seu retorno baseado em seus investimentos em marketing digital com apenas alguns cliques!

GERAR RELATÓRIO DE ROI
Compartilhe nas redes sociais:
  
Adicione aos favoritos:
Pressione CTRL + D para adicionar a página aos favoritos

Guru
POWERED BY

 WeDoLogos
 rockcontent
 x-tech commerce
Copyright © 2016 Guru PME. Todos os direitos reservados.
